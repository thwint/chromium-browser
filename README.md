# chromium browser #
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/thwint/chromium-browser/master)
![Docker Image Size (latest by date)](https://img.shields.io/docker/image-size/thwint/chromium-browser)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/thwint/chromium-browser)
![License MIT](https://img.shields.io/badge/license-MIT-blue.svg)

This docker image runs a chromium browser accessible using vnc.

## Quick start ##
Clone the git repository build and run the container

~~~bash
docker run --rm --name chromium -p 5900:5900 thwint/chromium-browser:latest
~~~

Connect to the container using vnc on port 5900. The default password is set to passwd

## Details ##
### Image ###
Based on a recent alpine linux image the image starts a X-server, a vnc server and a chrome browser.

#### Packages ####

* bash
* xvfb
* xdpyinfo
* x11vnc
* chromium

### docker-compose example ###

Sample configuration:

~~~yaml
version: '2.1'
services:
  chromium:
    image: thwint/chromium-browser
    container_name: chromium
    restart: always
    hostname: chromium
    networks:
      - mynet
    environment:
      BROWSER_URL: your.fancy.website.com
    ports:
      - 5900:5900

networks:
  guacamole:
    name: mynet
    external: true
~~~

## Customization ##
### VNC password ###
By default the VNC password is set to passwd. To override this password you can change the 
`PASSWD_FILE` environment variable and add the file to the container

~~~bash
docker run --rm --name chromium -p 5900:5900 \
-v /weherever/your/pass/is:/wherever/you/want/same_name_as_env \
-e PASSWD_FILE='/wherever/you/want/same_name_as_env'
thwint/chromium-browser:latest
~~~

### CHROMIUM_FLAGS ###
The chromium flags in this image are very specific for a test case. If you need another set of 
flags you can create your own file and replace the existing file in /etc/chromium/chromium.conf.

Current defaults:

`CHROMIUM_FLAGS="--disable-gpu --disable-software-rasterizer --disable-dev-shm-usage --kiosk --touch-events=enabled --no-sandbox --disable-features=TranslateUI"`

### Environment variables ###

| Variable name | default value              | description |
| ------------- | -------------------------- | ----------- |
| BROWSER_URL   |                            | The url to be opened when the browser starts.|
| PASSWD_FILE   | /home/baseuser/.vnc/passwd | The file containing the vnc password         |
| RESOLUTION    | 1024x768x24                | The resolution used for Xvfb                 |

There is no default startup url defined. But you can add an environment variable containing the 
desired startup url.
